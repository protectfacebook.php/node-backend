import {Pool} from 'pg'
import {Request, Response} from 'express'
import config from '../datasources/index'
const pool = new Pool(config.pooler)

const getUsers = async (req : Request, res : Response ): Promise<Response> =>{
    try {
        const response  =  await pool.query('select * from users')
        return res.status(200).json(response.rows)
    } catch (error) {
        return error; 
    }
}
const getUserById = async (req : Request, res : Response): Promise<Response> => {
    try{
        const  id  = req.params.id
        const response = await pool.query('select * from users where id = $1', [id])
        return res.status(200).json(response.rows)
    }catch(error){
        return error; 
    }
}

const createUser = async (req :Request, res : Response) : Promise<Response> => {
    try{
        const  uid = Date.now(); 
        const {name, email} = req.body; 
        const response  = await pool.query('insert into users(name, email, uid) values ($1, $2, $3)', [name, email, uid] )
        return(
            res.status(200).json({
                message : "User created",
                body : {
                    name,
                    email,
                    uid
                }
            })
        )
    }catch(err){
        return err;
    }
}

export  const users ={
    getUsers,
    getUserById,
    createUser
}
