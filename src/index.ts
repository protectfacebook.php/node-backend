import express from "express";
const app =  express();
import router from './routes/routes' 
import config from "./datasources/index"
app.use(express.json()); 
app.use(express.urlencoded({extended : false}))


app.use(router)




const server = app.listen(config.porter.port, ()=>{
    console.log(`server running in port  ${config.porter.port}`)
})