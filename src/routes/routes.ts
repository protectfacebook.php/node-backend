import {Router} from 'express'
const router = Router(); 
import {users} from '../controllers/UserController'


router.get('/users',  users.getUsers)
router.get('/users/:id', users.getUserById)
router.post('users', users.createUser)


export default router; 