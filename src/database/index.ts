import {Pool} from 'pg'
import config from '../datasources/index'
const pool = new Pool(config.pooler)


pool.on('connect', () => {
    console.log('connected to the db');
  });

const createTables = () => {
    const queryText =
      `CREATE TABLE IF NOT EXISTS
        users(
          id serial PRIMARY KEY,
          name VARCHAR(128) NOT NULL,
          email VARCHAR(128) NOT NULL,
          uid  VARCHAR(128) NOT NULL
        )`;
  
    pool.query(queryText)
      .then((res) => {
        console.log(res);
        pool.end();
      })
      .catch((err) => {
        console.log(err);
        pool.end();
      });
  }
  
export default createTables; 
import 'make-runnable'; 