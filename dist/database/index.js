"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const index_1 = __importDefault(require("../datasources/index"));
const pool = new pg_1.Pool(index_1.default.pooler);
pool.on('connect', () => {
    console.log('connected to the db');
});
const createTables = () => {
    const queryText = `CREATE TABLE IF NOT EXISTS
        users(
          id serial PRIMARY KEY,
          name VARCHAR(128) NOT NULL,
          email VARCHAR(128) NOT NULL,
          uid  VARCHAR(128) NOT NULL
        )`;
    pool.query(queryText)
        .then((res) => {
        console.log(res);
        pool.end();
    })
        .catch((err) => {
        console.log(err);
        pool.end();
    });
};
exports.default = createTables;
require("make-runnable");
