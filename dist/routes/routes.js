"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const UserController_1 = require("../controllers/UserController");
router.get('/users', UserController_1.users.getUsers);
router.get('/users/:id', UserController_1.users.getUserById);
router.post('users', UserController_1.users.createUser);
exports.default = router;
