"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = express_1.default();
const routes_1 = __importDefault(require("./routes/routes"));
const index_1 = __importDefault(require("./datasources/index"));
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: false }));
app.use(routes_1.default);
const server = app.listen(index_1.default.porter.port, () => {
    console.log(`server running in port  ${index_1.default.porter.port}`);
});
