"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const index_1 = __importDefault(require("../datasources/index"));
const pool = new pg_1.Pool(index_1.default.pooler);
const getUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield pool.query('select * from users');
        return res.status(200).json(response.rows);
    }
    catch (error) {
        return error;
    }
});
const getUserById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const id = req.params.id;
        const response = yield pool.query('select * from users where id = $1', [id]);
        return res.status(200).json(response.rows);
    }
    catch (error) {
        return error;
    }
});
const createUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const uid = Date.now();
        const { name, email } = req.body;
        const response = yield pool.query('insert into users(name, email, uid) values ($1, $2, $3)', [name, email, uid]);
        return (res.status(200).json({
            message: "User created",
            body: {
                name,
                email,
                uid
            }
        }));
    }
    catch (err) {
        return err;
    }
});
exports.users = {
    getUsers,
    getUserById,
    createUser
};
